package com.nespresso.sofa.recruitement.tournament.fighter.Equipement.defensive;

import com.nespresso.sofa.recruitement.tournament.fighter.Equipement.offensive.Axe;
import com.nespresso.sofa.recruitement.tournament.fighter.Equipement.offensive.Weapon;

public class Buckler implements Defensive {
    private int invocationTime = 0;
    private int timesHitedByAxe = 0;
    private Defensive defensive;

    public Buckler(Defensive defensive) {
        this.defensive = defensive;
    }

    public int defenseFromAttack(int damages, Weapon weapon) {
        if (damages > 0) {
            damages = reduceDamageFromOtherDefenseIfExist(damages, weapon);
            damages = reduceDamageIfUsable(damages, weapon);
        }
        return damages;
    }

    private int reduceDamageIfUsable(int damages, Weapon weapon) {
        if (isUsable()) {
            if (weapon instanceof Axe) {
                timesHitedByAxe++;
            }
            return 0;
        }
        return damages;
    }

    private int reduceDamageFromOtherDefenseIfExist(int damages, Weapon weapon) {
        if (defensive != null) {
            return defenseFromAttack(damages, weapon);
        }
        return damages;
    }

    private boolean isUsable() {
        return timesHitedByAxe < 3 && invocationTime++ % 2 == 0;
    }

    public int penaltiesOfUse() {
        return 0;
    }

    public void setPreviousDefensive(Defensive defensive) {
        if (defensive != null) defensive.setPreviousDefensive(this.defensive);
        this.defensive = defensive;
    }
}
