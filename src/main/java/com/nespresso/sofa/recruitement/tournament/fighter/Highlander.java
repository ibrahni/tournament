package com.nespresso.sofa.recruitement.tournament.fighter;

import com.nespresso.sofa.recruitement.tournament.fighter.Equipement.offensive.GreatSword;

public class Highlander extends Fighter {
    private final int INITIAL_HIT_POINT = 150;
    private boolean isVeteran;

    public Highlander() {
        this.weapon = new GreatSword();
        this.hitPoints = INITIAL_HIT_POINT;
    }

    public Highlander(String veteran) {
        this.weapon = new GreatSword();
        this.hitPoints = INITIAL_HIT_POINT;
        isVeteran = true;
    }

    @Override
    public int offense() {
        if (becomesABerserk()) {
            return super.offense() * 2;
        } else {
            return super.offense();
        }
    }

    private boolean becomesABerserk() {
        return isVeteran && (((double) hitPoints / (double) INITIAL_HIT_POINT) * 100 < 30);
    }

    @Override
    public Highlander equip(String equipment) {
        return (Highlander) super.equip(equipment);
    }
}
