package com.nespresso.sofa.recruitement.tournament.fighter.Equipement.defensive;

import com.nespresso.sofa.recruitement.tournament.fighter.Equipement.offensive.Weapon;

public class Armor implements Defensive {
    private Defensive defensive;

    public Armor(Defensive defensive) {
        this.defensive = defensive;
    }

    public int defenseFromAttack(int damages, Weapon weapon) {
        if (defensive != null) {
            damages = defensive.defenseFromAttack(damages, weapon);
        }
        return Math.max(0, damages - 3);
    }

    public int penaltiesOfUse() {
        return 1;
    }

    public void setPreviousDefensive(Defensive defensive) {
        if (defensive != null) defensive.setPreviousDefensive(this.defensive);
        this.defensive = defensive;
    }
}
