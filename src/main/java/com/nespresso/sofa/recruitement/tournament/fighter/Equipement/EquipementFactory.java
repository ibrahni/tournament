package com.nespresso.sofa.recruitement.tournament.fighter.Equipement;

import com.nespresso.sofa.recruitement.tournament.fighter.Equipement.defensive.Armor;
import com.nespresso.sofa.recruitement.tournament.fighter.Equipement.defensive.Buckler;
import com.nespresso.sofa.recruitement.tournament.fighter.Equipement.offensive.Axe;
import com.nespresso.sofa.recruitement.tournament.fighter.Fighter;

public class EquipementFactory {
    public static void createEquipementTo(String equipements, Fighter fighter) {
        if (equipements.equals("buckler")) {
            fighter.addDefensive(new Buckler(null));
        } else if (equipements.equals("armor")) {
            fighter.addDefensive(new Armor(null));
        }else if(equipements.equals("axe")){
            fighter.addOffensive(new Axe());
        }
    }
}
