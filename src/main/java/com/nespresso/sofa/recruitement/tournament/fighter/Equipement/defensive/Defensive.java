package com.nespresso.sofa.recruitement.tournament.fighter.Equipement.defensive;

import com.nespresso.sofa.recruitement.tournament.fighter.Equipement.offensive.Weapon;

public interface Defensive {
    int defenseFromAttack(int damages,Weapon weapon);
    int penaltiesOfUse();
    void setPreviousDefensive(Defensive defensive);
}
