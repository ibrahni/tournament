package com.nespresso.sofa.recruitement.tournament.fighter.Equipement.offensive;

public class Poison implements Weapon {
    private Weapon weapon;
    private int usedTimes = 0;

    public Poison(Weapon weapon) {
        this.weapon = weapon;
    }

    public int damage() {
        int damage = weapon.damage();
        if (isValid() && damage > 0) {
            return damage + 20;
        }
        return damage;
    }

    public boolean isValid() {
        usedTimes++;
        return (usedTimes <= 2);
    }
}
